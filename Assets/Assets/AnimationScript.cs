using System.Collections;
using System.Collections.Generic;
using Unity.VisualScripting;
using UnityEngine;
using UnityEngine.InputSystem;
using UnityEngine.UIElements;

public class PlayerController : MonoBehaviour
{
    public Animator _anim;
    private PlayerInput pInput;
    private Vector2 _position;
    private Rigidbody _rb;
    //private CapsuleCollider _capsuleCollider;
    public float _speed;
    public float sensibility;

    void Start()
    {

    }
    private void Awake()
    {
        _rb = GetComponent<Rigidbody>();
        pInput = GetComponent<PlayerInput>();
    }

    void Update()
    {

        _position = pInput.actions["Walk"].ReadValue<Vector2>();
        _rb.velocity = new Vector3(_position.x, 0, _position.y) * _speed;
        _anim.SetFloat("Velocity", _rb.velocity.magnitude);
        
    }
    private void OnEnable()
    {
        pInput.actions["Jump"].performed += Jump;
        pInput.actions["Run"].performed += Run;
        pInput.actions["Run"].canceled += DisableRun;
    }
    private void OnDisable()
    {

    }

    private void Jump(InputAction.CallbackContext ctx)
    {
        _anim.SetTrigger("Jump 0");
        _rb.velocity = new Vector3(_position.x, 5 ,_position.y) * _speed; //solucionar salto
    }

    private void Run(InputAction.CallbackContext ctx)
    {
        _speed *= 2f;
    }

    private void DisableRun(InputAction.CallbackContext ctx)
    {
        _speed /= 2f;
    }




}
